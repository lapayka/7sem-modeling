﻿#include <iostream>
#include <iomanip>
#include <fstream>

constexpr double m34 = 28395423107.0;
constexpr double m35 = 34359738368.0;
constexpr double m36 = 68719476736.0;
constexpr double m37 = 137438953472.0;

constexpr double step = 0.1;

double x;

double Rand(int n)
{
	
	if (n == 0)
	{
		x = m34;
		return 0.;
	}

	double S = 2.5;

	for (int i = 0; i < 5; i++)
	{
		x *= 5.;
		x = x > m37 ? x - m37 : x;
		x = x > m36 ? x - m36 : x;
		x = x > m35 ? x - m35 : x;

		double w = x / m35;

		if (n == 1)
		{
			return w;
		}
		
		S += w;
	}

	S *= 1.54919;

	return (S * S - 3.0) * S * 1e-2 + S;
}

#include <vector>

using namespace std;

vector<double> ders(const vector<double> & data)
{
	vector<double> res;
	res.push_back((3 * data[0] - 4 * data[1] + data[2]) / 2 / step);

	for (int i = 1; i < data.size() - 1; i++)
	{
		res.push_back((data[i + 1] - data[i - 1]) / 2. / step);
	}

	res.push_back((3 * data[data.size() - 1] + data[data.size() - 3] - 4 * data[data.size() - 2]) / 2 / step);

	return res;
}

double avg(const vector<double>& x)
{
	double sum = 0.;
	for (int i = 0; i < x.size(); i++)
	{
		sum += x[i];
	}

	return sum / x.size();
}

double corr(const vector<double>& x, const vector<double>& y)
{
	double x_avg = avg(x);
	double y_avg = avg(y);

	double numen = 0.;
	double denom_x = 0.;
	double denom_y = 0.;

	for (int i = 0; i < x.size(); i++)
	{
		double x_tmp = x[i] - x_avg;
		double y_tmp = y[i] - y_avg;
		numen += (x_tmp) * (y_tmp);
		denom_x += x_tmp * x_tmp;
		denom_y += y_tmp * y_tmp;
	}

	return numen / (sqrt(denom_x * denom_y));
}

double criterion(const vector<double>& data)
{
	vector<double> eve, odd;

	for (int i = 0; i < data.size(); i += 2)
	{
		eve.push_back(data[i]);
		odd.push_back(data[i + 1]);
	}

	return abs(corr(ders(eve), ders(odd)));
}

vector<double> read_table(int n)
{
	vector<double> res(n);

	ifstream stream("digits.txt");

	for (int i = 0; i < n && !stream.eof(); i++)
	{
		stream >> res[i];
	}

	return res;
}

int n = 10;

int main( void )
{
	Rand(0);

	std::cout << std::setw(15) << "Lec alg" << std::setw(15) << "Standart rand" << std::setw(15) << "Table" << std::endl;

	vector<double> lecR;
	vector<double> std;

	for (int i = 0; i < n; i++)
	{
		lecR.push_back(Rand(1));
		std.push_back(double(rand()) / RAND_MAX);
	}

	vector<double> table = read_table(n);

	for (int i = 0; i < lecR.size(); i++)
		std::cout << std::setw(15) << lecR[i]
			      << std::setw(15) << std[i]
				  << std::setw(15) << table[i]
				  << std::endl;


	std::cout << "Criterion" << std::endl
			  << std::setw(15) << criterion(lecR) << std::setw(15) << criterion(std) << std::setw(15) << criterion(table) << std::endl;

	{
		ofstream os("gr.csv");

		os << "N;Lec;Std;Table\n";
		for (int n = 100; n < 1001; n += 100)
		{
			vector<double> lecR;
			vector<double> std;

			for (int i = 0; i < n; i++)
			{
				lecR.push_back(Rand(1));
				std.push_back(double(rand()) / RAND_MAX);
			}

			vector<double> table = read_table(n);

			os << n << ";" << criterion(lecR) << ";" << criterion(std) << ";" << criterion(table) << "\n";
		}

	}
}
