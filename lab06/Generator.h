#pragma once
#include "Evaluator.h"
#include <memory>
#include <vector>
#include <queue>
#include <functional>

using namespace std;

class Generator :
    public Evaluator
{
private:
    shared_ptr<queue<double>> m_out;
public:
    Generator(double _time, double _delta, const shared_ptr<queue<double>>& out);

    virtual void timeTik(double time) override;
};

