#include "Commission.h"

constexpr double EPS = 1e-4;

Commission::Commission(double _time, double _delta, shared_ptr<vector<double>> _total_times, const shared_ptr<queue<double>>& in, const shared_ptr<queue<double>>& _first) : m_in(in), total_times(_total_times), first(_first)
{
	m_proccessor_time = _time;
	m_delta_time = _delta;
	cur_time = 0.;
	cur_query = 0.;
}

void Commission::timeTik(double time)
{
	if (abs(cur_time) > EPS)
		cur_time -= time;

	if (cur_time < EPS)
	{
		if (cur_query > EPS)
		{
			double p = double(rand()) / RAND_MAX;
			if (p < 0.1)
				first->push(cur_query);
			else if (p < 0.4)
				m_in->push(cur_query);
			else
			{
				total_times->push_back(current_time - cur_query);
				cur_query = 0.;
			}
		}

		if (!m_in->empty())
		{
			cur_query = m_in->front();
			m_in->pop();

			cur_time = generateTime();
		}
	}
}
