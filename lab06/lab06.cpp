#include <iostream>

#include "Processor.h"
#include "Generator.h"
#include "Commission.h"

double current_time = 0;
constexpr double EPS = 1e-4;

int main()
{
    vector<shared_ptr<Evaluator>> components;

    shared_ptr<queue<double>> pin = make_shared<queue<double>>();
    shared_ptr<queue<double>> psec = make_shared<queue<double>>();
    shared_ptr<vector<double>> total_times = make_shared<vector<double>>();

    components.push_back(make_shared<Generator>(40, 30, pin));
    components.push_back(make_shared<Proccessor>(30, 20, pin, psec));
    components.push_back(make_shared<Commission>(60, 30, total_times, psec, pin));

    while (total_times->size() != 120)
    {
        double min = DBL_MAX;
        for (const auto& elem : components)
        {
            if (!abs(elem->getCurTime()) < EPS && elem->getCurTime() < min)
                min = elem->getCurTime();
        }

        current_time += min;

        for (const auto& elem : components)
        {
            elem->timeTik(min);
        }

        //cout << total_times->size() << "\n";
    }


    double max = 0., avg = 0;
    
    for (const auto& elem : *total_times)
    {
        if (elem > max)
            max = elem;

        avg += elem;
    }

    avg /= total_times->size();

    cout << "Average time: " << avg << "minutes\n" << "Max time: " <<  max << "minutes\n";
}

