#pragma once
#include "Processor.h"

class Commission :
    public Evaluator
{
private:
    shared_ptr<queue<double>> m_in;

    shared_ptr<vector<double>> total_times;
    double cur_query;

    shared_ptr<queue<double>> first;

public:
    Commission(double _time, double _delta, shared_ptr<vector<double>> _total_times, const shared_ptr<queue<double>>& in, const shared_ptr<queue<double>>& _first);

    virtual void timeTik(double time) override;
};

