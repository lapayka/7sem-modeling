#include "Processor.h"

constexpr double EPS = 1e-4;

Proccessor::Proccessor(double _time, double _delta, const shared_ptr<queue<double>>& in, const shared_ptr<queue<double>>& out) : m_in(in), m_out(out)
{
	m_proccessor_time = _time;
	m_delta_time = _delta;
	cur_time = 0.;
	cur_query = 0.;
}

void Proccessor::timeTik(double time)
{
	if (abs(cur_time) > EPS)
		cur_time -= time;
	else
	{
		if (abs(cur_query) > EPS)
		{
			if (double(rand()) / RAND_MAX < 0.5)
				m_in->push(cur_query);
			else
			{
				m_out->push(cur_query);
				cur_query = 0.;
			}
		}
		if (!m_in->empty())
		{
			cur_query = m_in->front();
			m_in->pop();
			cur_time = generateTime();
		}
	}
}
