#include "Generator.h"
#include <queue>

constexpr double EPS = 1e-4;

Generator::Generator(double _time, double _delta, const shared_ptr<queue<double>> &out) : m_out(out)
{
	m_proccessor_time = _time;
	m_delta_time = _delta;
	cur_time = generateTime();
}

void Generator::timeTik(double time)
{
	cur_time -= time;

	if (abs(cur_time) < EPS)
	{
		cur_time = generateTime();
		m_out->push(current_time);
	}
}

