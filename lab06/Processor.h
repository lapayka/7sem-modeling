#pragma once
#include <queue>
#include <memory>
#include "Evaluator.h"

using namespace std;

class Proccessor : public Evaluator
{
private:
	shared_ptr<queue<double>> m_in;
	shared_ptr<queue<double>> m_out;
	double cur_query;

public:
	Proccessor(double _time, double _delta, const shared_ptr<queue<double>>& in, const shared_ptr<queue<double>> & out);

	virtual void timeTik(double time) override;
};

