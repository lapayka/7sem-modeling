// Lab04.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "EventModel.h"


#include <fstream>

constexpr double stime_step = 1e-1;

void smodeling(std::vector<std::pair<double, std::function<double()>>>& times)
{
    double generation_time = 0.0;
    double proccessing_time = 0.0;
    double stat_time = 0.0;
    for (double time = time_step; time < times[3].first; time += stime_step)
    {
        if (time > generation_time)
        {
            generation_time += times[0].second();
        }
        
        if (time > proccessing_time)
        {
            proccessing_time += times[1].second();
        }

        if (time > stat_time)
        {
            stat_time += times[2].second();
        }
    }
}

int main()
{
    std::ofstream stream("output2.csv");
    stream << "LAMBDA:SIZE" << "\n";

    for (lambda = 0.1; lambda < 3.; lambda += 0.1)
    {
        queue_size = 2;
        overflow = true;
        while (overflow)
        {
            overflow = false;
            std::queue<int> queue;
            Stat stat;

            std::vector<std::pair<double, std::function<double()>>> times = {
                                                                              {double(rand()) / RAND_MAX, [&]() {return generator(queue); } },
                                                                              {0., [&]() {return processing(queue, times); } },
                                                                              {time_step, [&]() {return stat_time(stat, queue, &times);  } },
                                                                              {1000.,[&]() { end = true; return 0.; }}
            };

            smodeling(times);

            //std::cout << "Queue size: " << queue_size << " Average queue size: " << stat.average_sum_of_sizes() << std::endl;

            queue_size += overflow;
        }

        std::cout << "lambda: " << lambda << " Queue size without overflow : " << queue_size << "\n";
        stream << lambda << ":" << queue_size << "\n";
    }
   
    //for (lambda = 0.1; lambda < 3.; lambda += 0.1)
    //{
    //    queue_size = 2;
    //    overflow = true;
    //    while (overflow)
    //    {
    //        overflow = false;
    //        std::queue<int> queue;
    //        Stat stat;
    //
    //        std::vector<std::pair<double, std::function<double()>>> times = {
    //                                                                          {double(rand()) / RAND_MAX, [&]() {return generator(queue); } },
    //                                                                          {0., [&]() {return processing(queue, times); } },
    //                                                                          {time_step, [&]() {return stat_time(stat, queue, &times);  } },
    //                                                                          {1000.,[&]() { end = true; return 0.; }}
    //        };
    //
    //        modeling(times);
    //
    //        //std::cout << "Queue size: " << queue_size << " Average queue size: " << stat.average_sum_of_sizes() << std::endl;
    //
    //        queue_size += overflow;
    //    }
    //
    //    std::cout << "lambda: " << lambda << " Queue size without overflow : " << queue_size << "\n";
    //    stream << lambda << ":" << queue_size << "\n";
    //}

    
    
}
