#include "EventModel.h"

double lambda = 0.1;
size_t queue_size = 1;
bool end = false;
bool overflow = true;

double min(const std::vector<std::pair<double, std::function<double()>>>& times)
{
    double tmp = DBL_MAX;
    for (const auto& elem : times)
    {
        if (elem.first < tmp && abs(elem.first) > eps)
            tmp = elem.first;
    }

    return tmp;
}

double generator(std::queue<int>& queue)
{
    if (queue.size() < queue_size)
        queue.push(0);
    else
    {
        overflow = true;
    }

    return double(rand()) / RAND_MAX;
}

double processing(std::queue<int>& queue, const std::vector<std::pair<double, std::function<double()>>>& times)
{
    if (queue.empty())
        return min(times);

    queue.pop();

    if (double(rand()) / RAND_MAX < p)
        queue.push(0);
    //std::cout << -1 / lambda * log(1 - double(rand()) / RAND_MAX) << "\n";
    return  -1 / lambda * log(1 - double(rand()) / RAND_MAX);
}

double stat_time(Stat& stat, std::queue<int>& queue, const std::vector<std::pair<double, std::function<double()>>>* times)
{
    stat.count++;
    stat.sum_of_sizes += queue.size();

    return time_step;
}



void time_tik(std::vector<std::pair<double, std::function<double()>>>& times)
{
    double _min = min(times);

    for (auto& elem : times)
    {
        if (abs(elem.first) > eps)
        {
            elem.first -= _min;

            if (abs(elem.first) < eps)
                elem.first = elem.second();
        }
        else
        {
            elem.first = elem.second();
        }
    }
}

void modeling(std::vector<std::pair<double, std::function<double()>>> times)
{
    end = false;
    while (!end)
    {
        time_tik(times);
    }
}