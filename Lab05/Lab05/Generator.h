#pragma once
#include "Evaluator.h"
#include <memory>
#include <vector>

using namespace std;

extern bool endOfModeling;
extern int missedQueries;
extern int totalQueries;

class Generator :
    public Evaluator
{
private:
    vector<shared_ptr<Evaluator>> m_proccessors;
public:
    Generator(double _time, double _delta, const vector<shared_ptr<Evaluator>> & proccessors);

    virtual void timeTik(double time) override;
};

