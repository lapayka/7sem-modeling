#pragma once

#include <stdlib.h>
#include <iostream>

class Evaluator
{
protected:
	double cur_time;
	double m_proccessor_time;
	double m_delta_time;

	virtual double generateTime() { return m_proccessor_time + ((double(rand()) / RAND_MAX) - 0.5) * m_delta_time; }

public:
	virtual double getCurTime() { return cur_time; }
	virtual void timeTik(double delta_time) { cur_time -= delta_time;  }
	virtual bool proccess() { return true; };
};

