#pragma once
#include <queue>
#include <memory>
#include "Evaluator.h"

using namespace std;

class Proccessor : public Evaluator
{
private:
	shared_ptr<queue<int>> m_out;
	bool is_busy;

public:
	Proccessor(double _time, double _delta, const shared_ptr<queue<int>> & out);

	virtual bool proccess() override;
	virtual void timeTik(double time) override;
};

