#include "Computer.h"

constexpr double EPS = 1e-4;

Computer::Computer(double _time, double _delta, const shared_ptr<queue<int>>& in): m_in(in)
{
	m_proccessor_time = _time;
	m_delta_time = _delta;
	cur_time = 0.;
}

void Computer::timeTik(double time)
{
	if (!abs(cur_time) < EPS)
		cur_time -= time;

	if (abs(cur_time) < EPS && !m_in->empty())
	{
		m_in->pop();
		cur_time = generateTime();
	}
}
