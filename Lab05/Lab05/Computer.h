#pragma once
#include "Evaluator.h"
#include <queue>
#include <memory>

using namespace std;

class Computer : public Evaluator
{
private:
    shared_ptr<queue<int>> m_in;

public:
    Computer(double _time, double _delta, const shared_ptr<queue<int>>& in);
    virtual void timeTik(double time) override;
};

