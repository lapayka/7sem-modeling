#include "Generator.h"

constexpr double EPS = 1e-4;

Generator::Generator(double _time, double _delta, const vector<shared_ptr<Evaluator>>& proccessors) : m_proccessors(proccessors)
{
	m_proccessor_time = _time;
	m_delta_time = _delta;
	cur_time = generateTime();
}

void Generator::timeTik(double time)
{
	cur_time -= time;

	if (abs(cur_time) < EPS)
	{
		cur_time = generateTime();
		bool all_busy = true;
		for (const auto& proccessor : m_proccessors)
		{
			if (proccessor->proccess())
			{
				all_busy = false;
				break;
			}
		}

		missedQueries += all_busy;
		if (++totalQueries == 300)
			endOfModeling = true;

	}
}

