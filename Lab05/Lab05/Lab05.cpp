#include <iostream>
#include "EventModel.h"

#include "Computer.h"
#include "Processor.h"
#include "Generator.h"

bool endOfModeling = false;
int missedQueries = 0;
int totalQueries = 0;

constexpr double EPS = 1e-4;

int main()
{
    shared_ptr<queue<int>> toFirstComp(make_shared<queue<int>>());
    toFirstComp->push(0);
    toFirstComp->pop();
    shared_ptr<queue<int>> toSecondComp(make_shared<queue<int>>());
    toSecondComp->push(0);
    toSecondComp->pop();


    shared_ptr<Evaluator> oper1(make_shared<Proccessor>(20, 5, toFirstComp));
    shared_ptr<Evaluator> oper2(make_shared<Proccessor>(40, 10, toFirstComp));
    shared_ptr<Evaluator> oper3(make_shared<Proccessor>(40, 20, toSecondComp));

    shared_ptr<Evaluator> gen(make_shared<Generator>(10, 2, vector<shared_ptr<Evaluator>>({oper1, oper2, oper3})));

    shared_ptr<Evaluator> comp1(make_shared<Computer>(15, 0, toFirstComp));
    shared_ptr<Evaluator> comp2(make_shared<Computer>(30, 0, toSecondComp));

    vector<shared_ptr<Evaluator>> components{ gen, oper1, oper2, oper3, comp1, comp2 };

    while (!endOfModeling)
    {
        double min = DBL_MAX;
        for (const auto& elem : components)
        {
            if (!abs(elem->getCurTime()) < EPS && elem->getCurTime() < min)
                min = elem->getCurTime();
        }

        for (const auto& elem : components)
        {
            elem->timeTik(min);
        }

        //cout << totalQueries << "\n";
    }

    cout << "Probability of miss: " << double(missedQueries) / totalQueries << "\n";
}

