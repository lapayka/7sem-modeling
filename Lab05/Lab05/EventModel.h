#pragma once


#include <iostream>
#include <vector>
#include <functional>
#include <float.h>
#include <queue>

extern double lambda;
extern size_t queue_size;
extern bool end;
extern bool overflow;

constexpr double time_step = 1e-1;
constexpr double eps = 1e-4;
constexpr double p = 0.3;



struct Stat
{
    int count = 0;
    int sum_of_sizes = 0;


    double average_sum_of_sizes() { return (double(sum_of_sizes)) / count; }
};

double min(const std::vector<std::pair<double, std::function<double()>>>& times);

double generator(std::queue<int>& queue)
	;

double processing(std::queue<int>& queue, const std::vector<std::pair<double, std::function<double()>>>& times)
;

double stat_time(Stat& stat, std::queue<int>& queue, const std::vector<std::pair<double, std::function<double()>>>* times)
;



void time_tik(std::vector<std::pair<double, std::function<double()>>>& times);


void modeling(std::vector<std::pair<double, std::function<double()>>> times)
;