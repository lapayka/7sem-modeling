#include "Processor.h"

constexpr double EPS = 1e-4;

Proccessor::Proccessor(double _time, double _delta, const shared_ptr<queue<int>>& out) : m_out(out)
{
	m_proccessor_time = _time;
	m_delta_time = _delta;
	cur_time = 0.;
	is_busy = false;
}

bool Proccessor::proccess()
{
	if (abs(cur_time) < EPS)
		return is_busy = true;
	else
		return false;
}

void Proccessor::timeTik(double time)
{
	if (abs(cur_time) > EPS)
		cur_time -= time;
	else if (is_busy)
	{
		cur_time = generateTime();
		is_busy = false;
	}
		
	
	if (abs(cur_time) < EPS)
	{
		m_out->push(0);
	}
	
}
