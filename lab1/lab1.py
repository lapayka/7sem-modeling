import matplotlib.pyplot as plt
from math import exp

def dens_uni_f(x, a, b):
    if (x < a or x > b):
        return 0
    else:
        return 1 / (b - a)

def dist_uni_f(x, a, b):
    if x < a:
        return 0
    elif x < b:
        return (x - a) / (b - a)
    else:
        return 1

def dens_exp_f(x, lamb):
    if (x < 0):
        return 0
    else:
        return lamb * exp(-lamb * x)

def dist_exp_f(x, lamb):
    if (x < 0):
        return 0
    else:
        return 1 - exp(-lamb * x)

#input of graphics
left_border = -1
right_border = 3
points_count = 100
step = (right_border - left_border) / points_count
x = []

i = left_border

while (i <= right_border):
    x.append(i)
    i += step

#distribution's parameters
a = 0
b = 1
lambdas = [0.5, 1, 1.5]

dens_uni = []
dist_uni = []
dens_exp = [[] for i in range(len(lambdas))]
dist_exp = [[] for i in range(len(lambdas))]
for i in x:
    dens_uni.append(dens_uni_f(i, a, b))
    dist_uni.append(dist_uni_f(i, a, b))

    if (dens_uni_f(i, a, b) == 0 and (dens_uni_f(i + step, a, b)) != 0):
        l = i
    if (dens_uni_f(i, a, b) != 0 and (dens_uni_f(i + step, a, b)) == 0):
        r = i

    if (i < 0 and i + step > 0):
        lex = i

    for j in range(len(lambdas)):
        dens_exp[j].append(dens_exp_f(i, lambdas[j]))
        dist_exp[j].append(dist_exp_f(i, lambdas[j]))



plt.plot(x, dens_uni)
plt.plot([l, l+step], [0, 1 / (b-a)], color = 'w')
plt.plot([r, r+step], [1 / (b-a), 0], color = 'w')
plt.xlabel("x")
plt.ylabel("f(x)")
plt.show()

plt.plot(x, dist_uni)
plt.xlabel("x")
plt.ylabel("F(x)")
plt.show()

for i in range(len(lambdas)):
    string = ("lambda = " + str(lambdas[i]))
    plt.plot(x, dens_exp[i], label = string)
    plt.plot([lex, lex + step], [0, dens_exp_f(lex + step, lambdas[i])], color = 'w')

plt.xlabel("x")
plt.ylabel("f(x)")
plt.legend()
plt.show()

for i in range(len(lambdas)):
    string = ("lambda = " + str(lambdas[i]))
    plt.plot(x, dist_exp[i], label = string)
    

plt.xlabel("x")
plt.ylabel("F(x)")
plt.legend()
plt.show()



