import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm


file = open("results.txt", "r")


times = list(map(float, file.readline().split()))


file.readline()

results = []
for line in file:
    results.append(list(map(float, line.split())))

colors = cm.rainbow(np.linspace(0, 1, len(results)))
i = 0
for result, color in zip(results, colors):
    i+=1
    plt.plot(times, result, label = "P" + str(i), color=color)
    
file.close()

file = open("points.txt", "r")

stab_times = list(map(float, file.readline().split()))
probs = list(map(float, file.readline().split()))

for t, p, color in zip(stab_times, probs, colors):
    plt.plot(t, p, marker = 'o', markeredgecolor=color, markerfacecolor=color)

file.close()

plt.legend()
plt.show()





