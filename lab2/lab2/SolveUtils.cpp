#include "SolveUtils.h"

double step = 1e-2;
constexpr double EPS = 1e-3;

double calc_der(const vec& coefs, const vec& args)
{
    double res = 0.;
    for (int j = 0; j < args.size(); j++)
        res += coefs[j] * args[j];
    return res;
}

matrix transpose(const matrix& mat)
{
    matrix res;
    for (int i = 0; i < mat[0].size(); i++)
    {
        vec tmp;
        for (int j = 0; j < mat.size(); j++)
            tmp.push_back(mat[j][i]);
        res.push_back(tmp);
    }

    return res;
}

std::pair<matrix, vec> solve(const matrix& system, double stop, int start_point)
{
    matrix res;
    vec times;
    {
        vec tem;
        --start_point;
        for (int i = 0; i < system.size(); i++)
            tem.push_back(double(i == start_point));

        res.push_back(tem);

        times.push_back(0.);
    }

    int i = 0;
    for (double t = step; t < stop; t += step, i++)
    {
        vec coefs1;
        vec coefs2;
        vec coefs3;
        vec coefs4;

        for (const auto& row : system)
            coefs1.push_back(step * calc_der(row, res[i]));
        {
            vec args = res[i];
            for (int j = 0; j < coefs1.size(); j++)
                args[j] += coefs1[j] / 2.;

            for (const auto& row : system)
                coefs2.push_back(step * calc_der(row, args));
        }

        {
            vec args = res[i];
            for (int j = 0; j < coefs2.size(); j++)
                args[j] += coefs2[j] / 2.;

            for (const auto& row : system)
                coefs3.push_back(step * calc_der(row, args));
        }

        {
            vec args = res[i];
            for (int j = 0; j < coefs3.size(); j++)
                args[j] += coefs3[j];

            for (const auto& row : system)
                coefs4.push_back(step * calc_der(row, args));
        }

        times.push_back(t);

        vec temp;
        for (int j = 0; j < coefs1.size(); j++)
            temp.push_back(res[i][j] + (coefs1[j] + 2 * coefs2[j] + 2 * coefs3[j] + coefs4[j]) / 6.);

        res.push_back(temp);
    }

    return make_pair(transpose(res), times);
}

void output_res(ofstream& stream, const matrix& mat, const vec& times)
{
    for (const auto& time : times)
        stream << time << " ";
    stream << "\n\n";

    for (const auto& row : mat)
    {
        for (const auto& elem : row)
            stream << elem << " ";
        stream << "\n";
    }
}

void output_res(ofstream& stream, const vec& times, const vec& probs)
{
    for (const auto& time : times)
        stream << time << " ";
    stream << "\n";

    for (const auto& prob : probs)
        stream << prob << " ";
    stream << "\n\n";
}

matrix create_system_from_graph(const matrix& graph)
{
    matrix res = graph;

    for (auto& row : res)
        for (auto& elem : row)
        {
            elem = 0.;
        }

    for (int i = 0; i < graph.size(); i++)
        for (int j = 0; j < graph[i].size(); j++)
        {
            res[i][i] -= graph[i][j];

            res[j][i] += graph[i][j];
        }

    return res;
}

double first_der(double left, double right, double step)
{
    return (right - left) / 2 / step;
}

double second_der(double left, double center, double right, double step)
{
    return (right - 2 * center + left) / (step * step);
}

std::pair<vec, vec> find_stab_times_and_probs(const matrix& func, const vec& times)
{
    vec stab_times;
    vec probs;

    for (int i = 0; i < func.size(); i++)
    {
        double stab_time = -1;
        double prob = -1;
        for (int j = 1; j < func[i].size() - 1; j++)
        {
            double fd = first_der(func[i][j - 1], func[i][j + 1], times[j] - times[j - 1]);
            double sd = second_der(func[i][j - 1], func[i][j], func[i][j + 1], times[j] - times[j - 1]);

            if (abs(fd) < EPS && abs(sd) < EPS)
            {
                stab_time = times[j];
                prob = func[i][j];
                break;
            }
        }
        stab_times.push_back(stab_time);
        probs.push_back(prob);
    }

    return make_pair(stab_times, probs);
}