#include "SolveUtils.h"

matrix init_matrix(int n)
{
    matrix res;
    for (int i = 0; i < n; i++)
    {
        vec tmp;
        for (int j = 0; j < n; j++)
            tmp.push_back(0);

        res.push_back(tmp);
    }

    return res;
}

void output_matrix(const matrix& mat)
{
    for (const auto& row : mat)
    {
        for (const auto& elem : row)
        {
            cout << elem << " ";
        }
        cout << "\n";
    }
    cout << std::endl;
}

pair<matrix, double> input_matrix(int &start_point)
{
    cout << "Input square matrix dimension : ";
    int n;
    cin >> n;

    matrix res = init_matrix(n);

    while (true)
    {
        system("cls");

        output_matrix(res);

        cout << "Input row index from 0 to " << n - 1 << " (-1 to continue)" << std::endl;
        int row;
        cin >> row;
        if (row < 0)
            break;

        cout << "Input column index from 0 to " << n - 1 << " (-1 to continue)" << std::endl;
        int col;
        cin >> col;

        cout << "Input value of matrix[" << row << ", " << col << "]" << std::endl;
        double val;
        cin >> val;

        res[row][col] = val;
    }

    cout << "Input boundaries of modeling from 0 to ";
    double stop;
    cin >> stop;

    cout << "Input the initial state number from 1 to " << n << std::endl;
    cin >> start_point;

    return make_pair(res, stop);
}

void output_results(const pair<vec, vec>& points)
{
    for (int i = 0; i < points.first.size(); i++)
    {
        cout << i + 1 << ": ";
        if (points.first[i] < 0)
            cout << "Can't find stabilization time in specified boundaries" << std::endl;
        else
            cout << "Stab time: " << points.first[i] << " Probability: " << points.second[i] << std::endl;
    }
}

int main()
{
    int start_point;
    auto p = input_matrix(start_point);
    matrix& graph = p.first;
    double stop = p.second;


    pair<matrix, vec> res = solve(create_system_from_graph(graph), stop, start_point);

    {
        ofstream stream("results.txt");
        output_res(stream, res.first, res.second);
    }

    pair<vec, vec> points = find_stab_times_and_probs(res.first, res.second);
    output_results(points);

    {
        ofstream stream("points.txt");
        output_res(stream, points.first, points.second);
    }

    system("python graph.py");

    return 0;
}
