#pragma once
#include <vector>
#include <functional>
#include <iostream>
#include <fstream>

using namespace std;

using vec = vector<double>;
using matrix = vector<vec>;

double calc_der(const vec& coefs, const vec& args);

matrix transpose(const matrix& mat);

std::pair<matrix, vec> solve(const matrix& system, double stop, int start_point);

void output_res(ofstream& stream, const matrix& mat, const vec& times);

void output_res(ofstream& stream, const vec& times, const vec& probs);

matrix create_system_from_graph(const matrix& graph);

double first_der(double left, double right, double step);

double second_der(double left, double center, double right, double step);

std::pair<vec, vec> find_stab_times_and_probs(const matrix& func, const vec& times);

